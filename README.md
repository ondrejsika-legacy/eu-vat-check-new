eu vat check
============

#### Author

* Ondrej Sika, <http://ondrejsika.com>, <ondrej@ondrejsika.com>


Docs
====

Example
-------

``` python
>>> from eu_vat_check import eu_vat_check

>>> eu_vat_check("CZ12345678")
(True, None)

>>> eu_vat_check("CZ12345678", "CZ87654321")
(True, "WAPIAAAAUMBlhgCf")
```
